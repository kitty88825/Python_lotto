import func

enter = True
print('歡迎來到大樂透啊！')
while enter:
    print('''
    請輸入遊玩方式
    1: 自動選號
    2: 手動選號
    ''')
    
    try:
        method = int(input('-> '))
        if method in [1, 2]:
            winning = func.auto_numbers()
            numbers = func.judge_method(method)
            same = func.confirm_the_same(winning, numbers)
            enter = False
        else:
            print('輸入錯誤，請重新輸入！！')
    except ValueError:
        print('輸入錯誤，請重新輸入！！')

result = '''
    開獎號碼：{}
    隨機號碼：{}
    中獎號碼：{}
    '''.format(winning, numbers, same)

print(result)