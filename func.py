import random


def auto_numbers():
	numbers = []
	while len(numbers) < 6:
		confirmed = random.randrange(1, 49)
		if confirmed not in numbers:
			numbers.append(confirmed)

	return numbers


def input_numbers():
	numbers = []
	while len(numbers) < 6:
		print('請輸入選擇的號碼(1~49)')
		user_input = input('-> ')
		if user_input.isdigit() and 1<=int(user_input)<=49:
			if int(user_input) not in numbers:
				numbers.append(int(user_input))
			else:
				print('已經重複囉')
		else:
			print('輸入錯誤！')
	return numbers


def confirm_the_same(winning, numbers):
	same = set(winning)& set(numbers)
	if not same:
		return '沒有中獎'
	else:
		return set(winning)& set(numbers)


def judge_method(method):
	if method == 1:
		numbers = auto_numbers()
	else:
		numbers = input_numbers()

	return numbers